var homeOneSubImg = ' <a href="images/realstate/realestate-1/Vallarta Cozy (1).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (2).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (3).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (4).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (5).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (6).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (7).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (8).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (9).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/realstate/realestate-1/Vallarta Cozy (10).jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>';

var homeTwoSubImg = '   <a href="images/realstate/realestate-2/Nuevo Property (1).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/realstate/realestate-2/Nuevo Property (2).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/realstate/realestate-2/Nuevo Property (3).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>';


var homeThreeSubImg = '<a href="images/realstate/realestate-3/Centro Condo (1).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/realstate/realestate-3/Centro Condo (2).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/realstate/realestate-3/Centro Condo (3).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/realstate/realestate-3/Centro Condo (4).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/realstate/realestate-3/Centro Condo (5).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/realstate/realestate-3/Centro Condo (6).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/realstate/realestate-3/Centro Condo (7).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/realstate/realestate-3/Centro Condo (8).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/realstate/realestate-3/Centro Condo (9).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>';

var homeFourSubImg = '<a href="images/realstate/realestate-4/Marina Vallarta (1).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/realstate/realestate-4/Marina Vallarta (2).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/realstate/realestate-4/Marina Vallarta (3).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/realstate/realestate-4/Marina Vallarta (4).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/realstate/realestate-4/Marina Vallarta (5).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/realstate/realestate-4/Marina Vallarta (6).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/realstate/realestate-4/Marina Vallarta (7).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/realstate/realestate-4/Marina Vallarta (8).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>';

$("#homeOne").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }

    $("#homeTwo").removeAttr("data-fancybox", "gallery");
    $("#homeThree").removeAttr("data-fancybox", "gallery");
    $("#homeFour").removeAttr("data-fancybox", "gallery");

    if ($("#homeOneSubImgs").children().length > 0) {
        $("#homeOneSubImgs").empty();
    }
    $("#homeOneSubImgs").append(homeOneSubImg);
    $("#homeTwoSubImgs").empty();
    $("#homeThreeSubImgs").empty();
    $("#homeFourSubImgs").empty();
});
$("#homeTwo").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }
    if ($("#homeTwoSubImgs").children().length > 0) {
        $("#homeTwoSubImgs").empty();
    }
    $("#homeOne").removeAttr("data-fancybox", "gallery");
    $("#homeThree").removeAttr("data-fancybox", "gallery");
    $("#homeFour").removeAttr("data-fancybox", "gallery");

    $("#homeTwoSubImgs").append(homeTwoSubImg);
    $("#homeOneSubImgs").empty();
    $("#homeThreeSubImgs").empty();
    $("#homeFourSubImgs").empty();
});
$("#homeThree").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }
    if ($("#homeThreeSubImgs").children().length > 0) {
        $("#homeThreeSubImgs").empty();
    }
    $("#homeOne").removeAttr("data-fancybox", "gallery");
    $("#homeTwo").removeAttr("data-fancybox", "gallery");
    $("#homeFour").removeAttr("data-fancybox", "gallery");
    $("#homeThreeSubImgs").append(homeThreeSubImg);
    $("#homeOneSubImgs").empty();
    $("#homeTwoSubImgs").empty();
    $("#homeFourSubImgs").empty();
});
$("#homeFour").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }
    if ($("#homeFourSubImgs").children().length > 0) {
        $("#homeFourSubImgs").empty();
    }
    $("#homeOne").removeAttr("data-fancybox", "gallery");
    $("#homeTwo").removeAttr("data-fancybox", "gallery");
    $("#homeThree").removeAttr("data-fancybox", "gallery");
    $("#homeFourSubImgs").append(homeThreeSubImg);
    $("#homeOneSubImgs").empty();
    $("#homeTwoSubImgs").empty();
    $("#homeThreeSubImgs").empty();
});