var homeOneSubImg = '  <a href="images/homerentals/home-1/rental vallarta countryside pool.jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/homerentals/home-1/rental vallarta countryside kitchen dining.jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/homerentals/home-1/rental vallarta countryside entrance.jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/homerentals/home-1/rental vallarta countryside entrance 2.jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>\
<a href="images/homerentals/home-1/rental vallarta countryside bedroom 2.jpg" data-fancybox="gallery" class="fancy-image home1-subimage"></a>';

var homeTwoSubImg = '  <a href="images/homerentals/home-2/Rental Condo (1).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (2).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (3).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (4).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (5).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (6).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (7).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (8).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (8) (1).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (8) (2).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (9).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (10).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (11).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (12).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (13).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>\
<a href="images/homerentals/home-2/Rental Condo (14).jpg" data-fancybox="gallery" class="fancy-image home2-subimage"></a>';


var homeThreeSubImg = '    <a href="images/homerentals/home-3/Spacey Condo (1).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/homerentals/home-3/Spacey Condo (2).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/homerentals/home-3/Spacey Condo (3).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/homerentals/home-3/Spacey Condo (4).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/homerentals/home-3/Spacey Condo (5) (1).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/homerentals/home-3/Spacey Condo (5).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/homerentals/home-3/Spacey Condo (6).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>\
<a href="images/homerentals/home-3/Spacey Condo (7).jpg" data-fancybox="gallery" class="fancy-image home3-subimage"></a>';

var homeFourSubImg = '<a href="images/homerentals/home-4/Amazing Vallarta Views (1).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/homerentals/home-4/Amazing Vallarta Views (2).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/homerentals/home-4/Amazing Vallarta Views (3).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/homerentals/home-4/Amazing Vallarta Views (4).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/homerentals/home-4/Amazing Vallarta Views (5).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/homerentals/home-4/Amazing Vallarta Views (6).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/homerentals/home-4/Amazing Vallarta Views (7).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>\
<a href="images/homerentals/home-4/Amazing Vallarta Views (8).jpg" data-fancybox="gallery" class="fancy-image home4-subimage"></a>';

$("#homeOne").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }

    $("#homeTwo").removeAttr("data-fancybox", "gallery");
    $("#homeThree").removeAttr("data-fancybox", "gallery");
    $("#homeFour").removeAttr("data-fancybox", "gallery");

    if ($("#homeOneSubImgs").children().length > 0) {
        $("#homeOneSubImgs").empty();
    }
    $("#homeOneSubImgs").append(homeOneSubImg);
    $("#homeTwoSubImgs").empty();
    $("#homeThreeSubImgs").empty();
    $("#homeFourSubImgs").empty();
});
$("#homeTwo").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }
    if ($("#homeTwoSubImgs").children().length > 0) {
        $("#homeTwoSubImgs").empty();
    }
    $("#homeOne").removeAttr("data-fancybox", "gallery");
    $("#homeThree").removeAttr("data-fancybox", "gallery");
    $("#homeFour").removeAttr("data-fancybox", "gallery");

    $("#homeTwoSubImgs").append(homeTwoSubImg);
    $("#homeOneSubImgs").empty();
    $("#homeThreeSubImgs").empty();
    $("#homeFourSubImgs").empty();
});
$("#homeThree").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }
    if ($("#homeThreeSubImgs").children().length > 0) {
        $("#homeThreeSubImgs").empty();
    }
    $("#homeOne").removeAttr("data-fancybox", "gallery");
    $("#homeTwo").removeAttr("data-fancybox", "gallery");
    $("#homeFour").removeAttr("data-fancybox", "gallery");
    $("#homeThreeSubImgs").append(homeThreeSubImg);
    $("#homeOneSubImgs").empty();
    $("#homeTwoSubImgs").empty();
    $("#homeFourSubImgs").empty();
});
$("#homeFour").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }
    if ($("#homeFourSubImgs").children().length > 0) {
        $("#homeFourSubImgs").empty();
    }
    $("#homeOne").removeAttr("data-fancybox", "gallery");
    $("#homeTwo").removeAttr("data-fancybox", "gallery");
    $("#homeThree").removeAttr("data-fancybox", "gallery");
    $("#homeFourSubImgs").append(homeThreeSubImg);
    $("#homeOneSubImgs").empty();
    $("#homeTwoSubImgs").empty();
    $("#homeThreeSubImgs").empty();
});