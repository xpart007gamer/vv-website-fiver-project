var carOneSubImg = '<a href="images/carrental/car1/2005_jeep_liberty_4dr-suv_limited_cc_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car1-subimage"></a>\
<a href="images/carrental/car1/2005_jeep_liberty_4dr-suv_limited_d_oem_1_500.jpg" data-fancybox="gallery" class="fancy-image car1-subimage"></a>\
<a href="images/carrental/car1/2005_jeep_liberty_4dr-suv_limited_ds_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car1-subimage"></a>\
<a href="images/carrental/car1/2005_jeep_liberty_4dr-suv_limited_fdd_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car1-subimage"></a>\
<a href="images/carrental/car1/2005_jeep_liberty_4dr-suv_limited_fdp_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car1-subimage"></a>';

var carTwoSubImg = '<a href="images/carrental/car2/2006_volkswagen_jetta_sedan_25_ds_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car2-subimage"></a>\
<a href="images/carrental/car2/2006_volkswagen_jetta_sedan_25_fcc_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car2-subimage"></a>\
<a href="images/carrental/car2/2006_volkswagen_jetta_sedan_25_fdd_evox_1_500.jpg" data-fancybox="gallery" class="fancy-image car2-subimage"></a>\
<a href="images/carrental/car2/2006_volkswagen_jetta_sedan_25_fs_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car2-subimage"></a>\
<a href="images/carrental/car2/2006_volkswagen_jetta_sedan_25_rps_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car2-subimage"></a>\
<a href="images/carrental/car2/2006_volkswagen_jetta_sedan_value-edition_hdd_evox_1_500.jpg" data-fancybox="gallery" class="fancy-image car2-subimage"></a>';

var carThreeSubImg = '  <a href="images/carrental/car3/2007_pontiac_solstice_convertible_gxp_fd_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car3-subimage"></a>\
<a href="images/carrental/car3/2007_pontiac_solstice_convertible_gxp_fs_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car3-subimage"></a>\
<a href="images/carrental/car3/2007_pontiac_solstice_convertible_gxp_i_oem_1_500.jpg" data-fancybox="gallery" class="fancy-image car3-subimage"></a>\
<a href="images/carrental/car3/2007_pontiac_solstice_convertible_gxp_pdo_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car3-subimage"></a>\
<a href="images/carrental/car3/2007_pontiac_solstice_convertible_gxp_pr_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car3-subimage"></a>\
<a href="images/carrental/car3/2007_pontiac_solstice_convertible_gxp_rqn_ei_1_500.jpg" data-fancybox="gallery" class="fancy-image car3-subimage"></a>';
$("#carOne").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox"); // attr = gallery
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }

    $("#carTwo").removeAttr("data-fancybox", "gallery");
    $("#carThree").removeAttr("data-fancybox", "gallery");

    if ($("#carOneSubImgs").children().length > 0) {
        $("#carOneSubImgs").empty();
    }
    $("#carOneSubImgs").append(carOneSubImg);
    $("#carTwoSubImgs").empty();
    $("#carThreeSubImgs").empty();
});
$("#carTwo").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }
    if ($("#carTwoSubImgs").children().length > 0) {
        $("#carTwoSubImgs").empty();
    }
    $("#carOne").removeAttr("data-fancybox", "gallery");
    $("#carThree").removeAttr("data-fancybox", "gallery");

    $("#carTwoSubImgs").append(carTwoSubImg);
    $("#carOneSubImgs").empty();
    $("#carThreeSubImgs").empty();
});
$("#carThree").click(function() {
    debugger;
    var attr = $(this).attr("data-fancybox");
    if (attr == null || attr == "" || typeof attr == 'undefined') {
        $(this).attr("data-fancybox", "gallery");
    }
    if ($("#carThreeSubImgs").children().length > 0) {
        $("#carThreeSubImgs").empty();
    }
    $("#carOne").removeAttr("data-fancybox", "gallery");
    $("#carTwoSubImgs").removeAttr("data-fancybox", "gallery");
    $("#carThreeSubImgs").append(carThreeSubImg);
    $("#carOneSubImgs").empty();
    $("#carTwoSubImgs").empty();
});